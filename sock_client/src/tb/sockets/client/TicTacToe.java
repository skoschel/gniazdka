package tb.sockets.client;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Painter;
import javax.swing.border.EmptyBorder;

public class TicTacToe extends JFrame implements Runnable, ActionListener {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe frame = new TicTacToe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private TicTacToe() {
		setTitle("K�ko i Krzy�yk | Multiplayer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);

		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 40, 14);
		contentPane.add(lblHost);

		JTextField frmtdtxtfldIp = new JTextField("localhost");
		frmtdtxtfldIp.setBounds(50, 11, 120, 20);
		contentPane.add(frmtdtxtfldIp);

		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 75, 23);
		contentPane.add(btnConnect);

		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("65000");
		frmtdtxtfldXxxx.setBounds(50, 39, 120, 20);
		contentPane.add(frmtdtxtfldXxxx);

		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 40, 14);
		contentPane.add(lblPort);

		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		add(lblNotConnected);

		lblStatus.setForeground(Color.BLACK);
		lblStatus.setOpaque(false);
		lblStatus.setBounds(10, 134, 123, 23);
		
		add(lblStatus);

		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnConnect.setEnabled(false);
				initTicTacToe(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
			}
		});

		p.setLayout(new GridLayout(3, 3));
		for (int i = 0; i < 9; i++) {
			buttons[i] = new XOButton();
			buttons[i].setSpace(i);
			buttons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (yourTurn && !unableToCommunicateWithOpponent && !won && !enemyWon && accepted) {

						int i = ((XOButton) e.getSource()).space;
						if (spaces[i] == null) {
							if (!circle) {
								spaces[i] = "X";
								buttons[i].set("X");
							} else {
								spaces[i] = "O";
								buttons[i].set("O");
							}
							// buttons[i].setEnabled(false);
							buttons[i].setBorderPainted(false);
							yourTurn = false;
							lblStatus.setText("Ruch przeciwnika");

							Toolkit.getDefaultToolkit().sync();

							try {
								dos.writeInt(i);
								dos.flush();
							} catch (IOException e1) {
								errors++;
								e1.printStackTrace();
							}

							System.out.println("DATA WAS SENT");
							checkForWin();
							checkForTie();


						}
					}
				}
			});

			p.add(buttons[i]);

		}
		
		p.setBounds(175, 0, getWidth() - 185, getHeight() - 50);
		add(p);

	}

	private JPanel p = new JPanel();
	XOButton buttons[] = new XOButton[9];

	private JPanel contentPane;
	JLabel lblNotConnected = new JLabel("Not Connected");
	JLabel lblStatus = new JLabel();

	private String ip = "localhost";
	private int port;

	private Thread thread;

	private Socket socket;
	private DataOutputStream dos;
	private DataInputStream dis;

	private ServerSocket serverSocket;

	private String[] spaces = new String[9];

	private boolean yourTurn = false;
	private boolean circle = true;
	private boolean accepted = false;
	boolean unableToCommunicateWithOpponent = false;
	private boolean won = false;
	private boolean enemyWon = false;
	private boolean tie = false;

	private int errors = 0;

	private String wonString = "Wygra�e�!";
	private String enemyWonString = "Zosta�e� upokorzony!";
	private String tieString = "Dramat, zremisowa�e�.";

	private int[][] wins = new int[][] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 },
			{ 0, 4, 8 }, { 2, 4, 6 } };

	/**
	 * <pre>
	 * 0, 1, 2 
	 * 3, 4, 5 
	 * 6, 7, 8
	 * </pre>
	 * 
	 * @param PORT
	 * @param IP
	 * @return
	 */

	public void initTicTacToe(String IP, int PORT) {

		ip = IP;
		port = PORT;

		System.out.println("Connecting to " + ip + ":" + port);

		if (!connect())
			initializeServer();

		thread = new Thread(this, "TicTacToe");
		thread.start();
	}

	public void run() {
		while (true) {
			tick();

			if (!circle && !accepted) {
				listenForServerRequest();
			}
			
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void tick() {
//		System.out.print("");
		if (errors >= 10) {
			unableToCommunicateWithOpponent = true;

			lblNotConnected.setBackground(Color.RED);
			lblNotConnected.setText("Utracono po��czenie");
			lblNotConnected.setForeground(Color.black);
			// System.out.println("LOST CONNECT WITH CLIENT");
		}

		// poprawne polaczenie, oczekiwanie na ruch przeciwnika
		if (!yourTurn && !unableToCommunicateWithOpponent) {
			try {
				int space = dis.readInt();
				System.out.println(space);
				if (circle) {
					spaces[space] = "X";
					buttons[space].set("X");
					buttons[space].setBorderPainted(false);
				} else {
					spaces[space] = "O";
					buttons[space].set("O");
					buttons[space].setBorderPainted(false);
				}

				// sprawdzanie czy koniec gry
				yourTurn = true;
				lblStatus.setText("Tw�j ruch");

				checkForEnemyWin();
				checkForTie();

			} catch (IOException e) {
				e.printStackTrace();
				errors++;
			}
		}
	}

	private void listenForServerRequest() {
		Socket socket = null;
		try {
			socket = serverSocket.accept();
			dos = new DataOutputStream(socket.getOutputStream());
			dis = new DataInputStream(socket.getInputStream());
			accepted = true;

			lblNotConnected.setBackground(Color.GREEN);
			lblNotConnected.setText("Po��czono");
			lblNotConnected.setForeground(Color.black);
			System.out.println("CLIENT HAS REQUESTED TO JOIN, AND WE HAVE ACCEPTED");
			setButtons(true);
			lblStatus.setText("Tw�j ruch");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean connect() {
		try {
			socket = new Socket(ip, port);
			dos = new DataOutputStream(socket.getOutputStream());
			dis = new DataInputStream(socket.getInputStream());
			accepted = true;
		} catch (IOException e) {
			System.out.println("Unable to connect to the address: " + ip + ":" + port + " | Starting a server");
			lblNotConnected.setBackground(Color.YELLOW);
			lblNotConnected.setText("Oczekwianie na przeciwnika");
			lblNotConnected.setForeground(Color.black);
			return false;
		}

		lblNotConnected.setBackground(Color.GREEN);
		lblNotConnected.setText("Po��czono");
		lblNotConnected.setForeground(Color.black);
		System.out.println("Successfully connected to the server.");
		setButtons(true);
		lblStatus.setText("Ruch przeciwnika");
		return true;
	}

	private void setButtons(boolean enabled)
	{
		for (int i = 0; i < 9; i++) {
			buttons[i].setEnabled(enabled);
		}
	}
	
	private void initializeServer() {
		try {
			serverSocket = new ServerSocket(port, 8, InetAddress.getByName(ip));
		} catch (Exception e) {
			e.printStackTrace();
		}
		yourTurn = true;
		circle = false;
	}

	private void checkForWin() {
		String signPlayer;
		if (circle) {
			signPlayer = "O";
		} else
			signPlayer = "X";

		for (int i = 0; i < wins.length; i++) {
			if (spaces[wins[i][0]] == signPlayer && spaces[wins[i][1]] == signPlayer
					&& spaces[wins[i][2]] == signPlayer) {
				won = true;
				lblStatus.setForeground(Color.GREEN);
				lblStatus.setFont(new Font("Verdana", Font.BOLD, 36));
				lblStatus.setText(wonString);
				lblStatus.setBounds(getWidth()/2-30, 150, 250, 150);
				
				setButtons(false);
			}
		}
	}

	private void checkForEnemyWin() {
		String signPlayer;
		if (!circle) {
			signPlayer = "O";
		} else
			signPlayer = "X";

		for (int i = 0; i < wins.length; i++) {
			if (spaces[wins[i][0]] == signPlayer && spaces[wins[i][1]] == signPlayer
					&& spaces[wins[i][2]] == signPlayer) {
				enemyWon = true;
				
				lblStatus.setForeground(Color.BLACK);
				lblStatus.setFont(new Font("Verdana", Font.BOLD, 36));
				lblStatus.setText(enemyWonString);
				lblStatus.setBounds(getWidth()/2-150, 150, 500, 150);
				setButtons(false);
			}
		}
	}

	private void checkForTie() {
		for (int i = 0; i < spaces.length; i++) {
			if (spaces[i] == null) {
				return;
			}
		}
		lblStatus.setForeground(Color.BLACK);
		lblStatus.setBounds(getWidth()/2-150, 150, 500, 150);
		lblStatus.setFont(new Font("Verdana", Font.BOLD, 36));
		lblStatus.setText(tieString);
		setButtons(false);
		tie = true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
