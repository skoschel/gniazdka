package tb.sockets.client;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.awt.Color;
import java.awt.event.ActionEvent;

public class XOButton extends JButton implements MouseListener {
	ImageIcon X, O;
//	byte value = 0;
	String value = null;
	public int space;
	/*
	 * 0:nothing 1:X 2:O
	 */

	public XOButton() {
		X = new ImageIcon(this.getClass().getResource("/X.png"));
		O = new ImageIcon(this.getClass().getResource("/O.png"));
		this.setBackground(Color.WHITE);
//		this.addActionListener(this);
	}
	
	public void setSpace(int space)
	{
		this.space = space;
	}

	public void set(String value) {
		switch (value) {
		case "X":
			setIcon(X);
			break;
		case "O":
			setIcon(O);
			break;
		default:
			setIcon(null);
			break;
		}
	}
	
	public String get()
	{
		return value;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

//	public void actionPerformed(ActionEvent e) {
//		value++;
//		value %= 3;
//		switch (value) {
//		case 0:
//			setIcon(null);
//			break;
//		case 1:
//			setIcon(X);
//			break;
//		case 2:
//			setIcon(O);
//			break;
//		}
//	}
}